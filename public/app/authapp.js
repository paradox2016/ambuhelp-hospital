var loginApp = angular.module('LoginAuthApp', ['ui.bootstrap', 'ngCookies', 'ngToast','lbServices']);

loginApp.controller('authController', function ($scope, $cookieStore, $window, ngToast, HospitalModel, LoopBackAuth) {

    $scope.loginInProgress = false;

    if(LoopBackAuth.currentUserId !== null){
      $window.location.href = '/dashboard/#';
    }

    $scope.doLogin = function () {
        // implementation of login Logic
        //RestAPI call for login
        $scope.lgnFailedMsg = null;
        $scope.loginInProgress = true;
        $scope.loginResult = HospitalModel.login($scope.login, function (res) {
                $scope.loginInProgress = false;

                $window.location.href = '/dashboard/#';
            },
            function (res) {
                $scope.loginInProgress = false;
                $scope.login.username = '';
                $scope.login.password = '';
                $scope.lgnFailedMsg = 'username or password is wrong';
            });

    };



});
