angular.module('LoopbackSocketIntegration', [])

.factory('socket',function(socketFactory,LoopBackAuth,$rootScope){
	//Create socket and connect to http://chat.socket.io 
    $rootScope.isSocketConnected = false;
  	socket = socketFactory({
    	ioSocket: io.connect('http://ambuhelp-server.mybluemix.net')
        /*ioSocket: io.connect('http://localhost:3000')*/
  	});
    

    socket.on('connect', function () {
        console.log('socket on connect fired');
        var userId = LoopBackAuth.currentUserId;
        socket.emit('new-user', {id: userId,type: 'hospital',location:{}});

    });
    
    socket.on('new-user-connected',function(){
        console.log('new user registerd');
        $rootScope.isSocketConnected = true;
    })
  	
	return socket;
})

.factory('PubSub', function (socket) {
    var container = [];
    return {
        unSubscribeThis : function(options){
            if(options){
                var name = "";
                var collectionName = options.collectionName;
                var modelId = options.modelId;
                var method = options.method;
                if(method === 'POST'){
                    name = '/' + collectionName + '/' + method;
                }
                else{
                    name = '/' + collectionName + '/' + modelId + '/' + method;
                }
                
                angular.forEach(container,function(value,index){
                    if(name === value){
                        socket.removeAllListeners(value);
                        container.splice(index,1);
                    }
                });
            }
            
        },
        subscribe: function(options, callback){
            if(options){
                var collectionName = options.collectionName;
                var modelId = options.modelId;
                var method = options.method;
                
                if(method === 'POST'){
                    var name = '/' + collectionName + '/' + method;
                }
                else{
                    var name = '/' + collectionName + '/' + modelId + '/' + method;
                }
                this.unSubscribeThis(options);
                socket.on(name, callback);
                //Push the container..
                
                this.pushContainer(name);
                console.log(container)
            }else{
                throw 'Error: Option must be an object';
            }
        }, //end subscribe

        pushContainer: function (subscriptionName) {
            container.push(subscriptionName);
        },
        
        

        //Unsubscribe all containers..
        unSubscribeAll: function () {
            for (var i = 0; i < container.length; i++) {
                socket.removeAllListeners(container[i]);
            }
            //Now reset the container..
            container = [];
        }

    };
});