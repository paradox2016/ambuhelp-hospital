var app = angular.module('ambuhelpHospitalApp', ['ui.bootstrap', 'ui.router', 'ngCookies', 'lbServices', 'toaster', 
'ngAnimate', 'ngAudio', 'btford.socket-io', 'LoopbackSocketIntegration','map.services','camelCaseToHuman','pascalprecht.translate', 'humanSeconds','angular-image-404']);

app.config(function ($stateProvider, $urlRouterProvider) {

    // For unmatched routes
    $urlRouterProvider.otherwise('/');

    // Application routes
    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: 'home/home.html',
            controller: 'homeController',
            onExit: unSubscribeAll
        })
        .state('patient', {
            url: '/patient',
            templateUrl: 'patient/patientInfo.html',
            controller: 'patientController'
        })
        .state('contact', {
            url: '/contact',
            templateUrl: 'contact/contactPage.html',
            controller: 'contactController'
        })
        .state('doctors', {
            url: '/doctors',
            templateUrl: 'doctors/doctors.html',
            controller: 'docController'
        })
        .state('userProfile', {
            url: '/userProfile',
            templateUrl: 'userProfile/profileModal.html',
            controller: 'profileController'
        })
});

app.controller('MasterCtrl', function ($scope, $rootScope, $cookieStore, HospitalModel, LoopBackAuth, ngAudio, socket) {


    if ($rootScope.hospitalDetails === null || angular.isUndefined($rootScope.hospitalDetails)) {
        //Get hospitalDetails for corresponding Hospital id
        HospitalModel.hospitalDetails({
            id: LoopBackAuth.currentUserId
        }).$promise.then(function (hospitalDetails) {
            $rootScope.hospitalDetails = hospitalDetails;
            socket.emit('update-location', {
                id: LoopBackAuth.currentUserId,
                userType: 'hospital',
                location: $rootScope.hospitalDetails.mapLocation
            });
        }, function (err) {
            console.log(err);
        });
    }


    /**
     * Sidebar Toggle & Cookie Control
     * $scope is the application object => the owner of Application variable and functions
     */
    var mobileView = 992;

    $rootScope.notificationSound = ngAudio.load("../assets/sound/notification.mp3");

    $scope.getWidth = function () {
        return window.innerWidth;
    };

    $scope.$watch($scope.getWidth, function (newValue, oldValue) {
        if (newValue >= mobileView) {
            if (angular.isDefined($cookieStore.get('toggle'))) {
                $scope.toggle = !$cookieStore.get('toggle') ? false : true;
            } else {
                $scope.toggle = true;
            }
        } else {
            $scope.toggle = false;
        }

    });

    $scope.toggleSidebar = function () {
        $scope.toggle = !$scope.toggle;
        $cookieStore.put('toggle', $scope.toggle);
    };

    window.onresize = function () {
        $scope.$apply();
    };
    $scope.logout = function () {
        socket.disconnect();
        HospitalModel.logout().$promise.then(function (response) {
            window.location.href = '/';
        }, function (err) {
            console.log('logout failed');
        })

    };

    $rootScope.sosRequests = {};


});

var unSubscribeAll = function(PubSub){
    //Unsubscribe all listeners..
    PubSub.unSubscribeAll();
}
