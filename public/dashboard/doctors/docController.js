app.controller('docController', function ($scope) {
    $scope.doctors = [{
            "name": "Cheryl Gilbert",
            "from": "5:45 AM",
            "to": "8:29 AM",
            "Gender": "Female",
            "Specialty": "Pediatric Surgery"
        },
        {
            "name": "Gerald Spencer",
            "from": "11:37 AM",
            "to": "2:50 AM",
            "Gender": "Male",
            "Specialty": "Vascular Surgery"
        },
        {
            "name": "Pamela Peterson",
            "from": "4:52 PM",
            "to": "3:43 AM",
            "Gender": "Female",
            "Specialty": "Neurologic Surgery"
        },
        {
            "name": "Benjamin Carter",
            "from": "1:00 AM",
            "to": "7:44 AM",
            "Gender": "Male",
            "Specialty": "Gynecology Oncology"
        },
        {
            "name": "Linda Howard",
            "from": "12:17 PM",
            "to": "2:39 AM",
            "Gender": "Female",
            "Specialty": "Maternal Fetal Medicine"
        },
        {
            "name": "Ashley Dunn",
            "from": "4:08 PM",
            "to": "2:41 PM",
            "Gender": "Female",
            "Specialty": "Female Pelvic Medicine and Reconstructive Surgery"
        },
        {
            "name": "Billy Howell",
            "from": "2:54 PM",
            "to": "6:58 PM",
            "Gender": "Male",
            "Specialty": "Orthopaedic Surgery"
        },
        {
            "name": "Billy Ramos",
            "from": "6:32 PM",
            "to": "2:57 PM",
            "Gender": "Male",
            "Specialty": "Urology Surgeon"
        },
        {
            "name": "Kathleen Evans",
            "from": "3:27 AM",
            "to": "9:11 PM",
            "Gender": "Female",
            "Specialty": "Orthopaedic Surgery"
        },
        {
            "name": "Nicole Anderson",
            "from": "11:16 AM",
            "to": "2:35 AM",
            "Gender": "Female",
            "Specialty": "Oral and Maxillofacial Surgery"
        },
        {
            "name": "Rebecca Long",
            "from": "2:41 AM",
            "to": "1:04 AM",
            "Gender": "Female",
            "Specialty": "Orthopaedic Surgery"
        },
        {
            "name": "Diane Cox",
            "from": "10:00 AM",
            "to": "8:15 AM",
            "Gender": "Female",
            "Specialty": "Ophthalmologic Surgery"
        },
        {
            "name": "Wayne Armstrong",
            "from": "2:27 AM",
            "to": "4:23 AM",
            "Gender": "Male",
            "Specialty": "Pediatric Otolaryngology"
        },
        {
            "name": "Michael Reed",
            "from": "2:51 AM",
            "to": "12:42 PM",
            "Gender": "Male",
            "Specialty": "Ophthalmologic Surgery"
        },
        {
            "name": "Jean King",
            "from": "3:28 AM",
            "to": "10:36 PM",
            "Gender": "Female",
            "Specialty": "Neurologic Surgery"
        },
        {
            "name": "Carl Sullivan",
            "from": "6:47 AM",
            "to": "3:41 PM",
            "Gender": "Male",
            "Specialty": "Endocrinologist"
        },
        {
            "name": "Melissa Kennedy",
            "from": "7:27 AM",
            "to": "3:46 PM",
            "Gender": "Female",
            "Specialty": "Pulmonologist"
        },
        {
            "name": "Bonnie Washington",
            "from": "6:59 PM",
            "to": "7:56 PM",
            "Gender": "Female",
            "Specialty": "Neurologic Surgery"
        },
        {
            "name": "Helen Kelley",
            "from": "11:59 AM",
            "to": "2:12 AM",
            "Gender": "Female",
            "Specialty": "Pulmonologist"
        },
        {
            "name": "Raymond Romero",
            "from": "5:23 PM",
            "to": "6:14 AM",
            "Gender": "Male",
            "Specialty": "Orthopaedic Surgery"
        }];

});
