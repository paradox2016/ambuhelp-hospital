app.controller('contactController', function ($scope, $rootScope, HospitalModel, LoopBackAuth,MapUtility) {
    //Get hospitalDetails for corresponding Hospital id if contact details is called directly before dashboard
    var mapDiv = document.getElementById("map");
    
    var mapIcons = {
            hospitalIcon: '../assets/img/hospital_map_icon.png',
            ambulanceIcon: '../assets/img/ambulance_map_marker.png',
            sosIcon: '../assets/img/sos_marker.png'
    }
    
    $scope.hospitalMap = function () {
        $scope.hospitalDetailsCopy = $rootScope.hospitalDetails;

        var myLatlng = new google.maps.LatLng($scope.hospitalDetailsCopy.mapLocation.lat, $scope.hospitalDetailsCopy.mapLocation.lng);
        var mapOptions = MapUtility.getDefaultMapOption(myLatlng);
        
        $scope.map  = new google.maps.Map(mapDiv, mapOptions);
        //Wait until the map is loaded
        google.maps.event.addListenerOnce($scope.map, 'idle', function () {
            var marker = MapUtility.putMarker($scope.map, myLatlng, MapUtility.getMapIcon(mapIcons.hospitalIcon, 48))
        });
                                          
    }
    
    if ($rootScope.hospitalDetails === null || angular.isUndefined($rootScope.hospitalDetails)) {
        HospitalModel.hospitalDetails({
            id: LoopBackAuth.currentUserId
        }).$promise.then(function (hospitalDetails) {
            $rootScope.hospitalDetails = hospitalDetails;
             socket.emit('update-location',{id:LoopBackAuth.currentUserId, userType:'hospital',location:$rootScope.hospitalDetails.mapLocation});
            $scope.hospitalMap();
        }, function (err) {
            console.log(err);
        });
    } else {
        $scope.hospitalMap();
    }

    $scope.editDetails = false;
    $scope.enableEditing = function () {
        $scope.dbHospitalDetailsCopy = $rootScope.hospitalDetails;
        console.log($rootScope.hospitalDetails);
        $scope.editDetails = true;
    };

    $scope.saveDetails = function () {
        HospitalModel.hospitalDetails.update({
            id: LoopBackAuth.currentUserId
        }, $scope.hospitalDetails).$promise.then(
            function (res) {
                console.log(res);
                $scope.dbHospitalDetailsCopy = $scope.hospitalDetails;
                $scope.editDetails = false;

            },
            function (error) {})
    }

    $scope.cancleEdit = function () {
        $scope.hospitalDetails = angular.copy($scope.dbHospitalDetailsCopy);
        $scope.editDetails = false;
    };

    $rootScope.$on("callDataForMap", function () {
        $scope.dataForMap();
    });


})
