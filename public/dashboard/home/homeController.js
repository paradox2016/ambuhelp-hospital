app.controller('homeController', function ($scope, $rootScope, toaster, HospitalModel, UserModel, LoopBackAuth, socket, PubSub, MapUtility) {
   
    
    var generateRandom = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    
    $scope.availableDoctors = generateRandom(5, 20);
    $scope.availableBeds = generateRandom(20, 50);
    $scope.availablePatients = generateRandom(20, 60);
    $scope.admittedPatients = {};
    $scope.incomingPatients = {};
    
    $scope.incomingRequests = [];

    $scope.ifValidKey = function (key) {
        return (key.toLowerCase().indexOf('id') == -1)
    }
    
    var occupiedWard = [];
    var getRandomWard = function(){
        var ward = undefined;
        
        do{
            ward =  generateRandom(20,$scope.availableBeds);
        }while(occupiedWard.indexOf(ward) !== -1);
        $scope.availableBeds--;
        occupiedWard.push(ward);
        return ward;
    }

    $scope.findCriticalState = function (state) {
        var className = "";
        if (state) {
            switch (state.toLowerCase()) {
            case 'critical':
                className = 'text-danger';
                break;
            case 'high':
                className = 'text-warning';
                break;
            case 'normal':
                className = 'text-primary';
                break;
            case 'low':
                className = 'text-success';
                break;
            }
        }

        return className;
    }
    
    /*Map Utils & variables*/
    
     //Marker hash map declaration
    var mapMarkers = {};

    var mapDiv = document.getElementById("map");


    var mapIcons = {
        hospitalIcon: '../assets/img/hospital_map_icon.png',
        ambulanceIcon: '../assets/img/ambulance_map_marker.png',
        sosIcon: '../assets/img/sos_marker.png'
    }
    
    var bounds = new google.maps.LatLngBounds();
    
    var fitToBounds = function (position) {
        var diagLat = 2 * $scope.hospitalLatlng.lat() - position.lat();
        var diagLng = 2 * $scope.hospitalLatlng.lng() - position.lng();
        var oppositePos = new google.maps.LatLng(diagLat, diagLng);

        bounds.extend(position);
        bounds.extend(oppositePos);

        $scope.map.fitBounds(bounds);

        //remove one zoom level to ensure no marker is on the edge.
        $scope.map.setZoom($scope.map.getZoom() - 1);

        // set a minimum zoom 
        // if you got only 1 marker or all mapMarkers are on the same address map will be zoomed too much.
        if ($scope.map.getZoom() > 15) {
            $scope.map.setZoom(15);
        }

    }
    var calculateDuration = function(routes,bookingDetail){
        bookingDetail.route = {
            parts : [],
            totalDuration: 0
        }
        angular.forEach(routes,function(route,index){
           bookingDetail.route.parts.push(route.duration);
           bookingDetail.route.totalDuration += route.duration.value;
        });
    }
    
    var getDuration = function (bookingDetail,isPicked) {
        
        var directionsService = new google.maps.DirectionsService();
        var request = {
            destination: new google.maps.LatLng(bookingDetail.hospital.hospitalDetails.mapLocation.lat, bookingDetail.hospital.hospitalDetails.mapLocation.lng),
            waypoints: [],
            unitSystem: google.maps.UnitSystem.METRIC,
            travelMode: google.maps.TravelMode.DRIVING
        };
        
        if(isPicked){
            request.origin = new google.maps.LatLng(bookingDetail.user.location.lat, bookingDetail.user.location.lng);
        } else{
            request.origin = new google.maps.LatLng(bookingDetail.ambulance.location.lat, bookingDetail.ambulance.location.lng);
            request.waypoints.push({
                location:new google.maps.LatLng(bookingDetail.user.location.lat, bookingDetail.user.location.lng),
                stopover: true
            })
        }
        
        directionsService.route(request, function(result, status) {
        if (status == 'OK') {
            calculateDuration(result.routes[0].legs,bookingDetail);
         }
       });

    }
    
    

    var hospitalMap = function () {
        $scope.hospitalLatlng = new google.maps.LatLng($rootScope.hospitalDetails.mapLocation.lat, $rootScope.hospitalDetails.mapLocation.lng);
        var mapOptions = MapUtility.getDefaultMapOption($scope.hospitalLatlng);
        $scope.map = new google.maps.Map(mapDiv, mapOptions);
        directionsDisplay = new google.maps.DirectionsRenderer();
        directionsDisplay.setMap($scope.map);
        var hospitalMarker = MapUtility.putMarker($scope.map, $scope.hospitalLatlng, MapUtility.getMapIcon(mapIcons.hospitalIcon, 48))
        return hospitalMarker;
    };

    
    var putSOSMarker = function(id,location){
        var markerPos = new google.maps.LatLng(location.lat, location.lng);
        
        var sosMarker = MapUtility.putMarker($scope.map, markerPos, MapUtility.getMapIcon(mapIcons.sosIcon, 36));
        mapMarkers[id] = sosMarker;
        fitToBounds(markerPos);
        return sosMarker;
    }
    
    $scope.initMap = function () {
        
        if ($rootScope.hospitalDetails === null || angular.isUndefined($rootScope.hospitalDetails)) {
            //Get Hospital Details using lbservices
            HospitalModel.hospitalDetails({
                id: LoopBackAuth.currentUserId
            }).$promise.then(function (hospitalDetails) {
                $rootScope.hospitalDetails = hospitalDetails;
                socket.emit('update-location', {
                    id: LoopBackAuth.currentUserId,
                    userType: 'hospital',
                    location: $rootScope.hospitalDetails.mapLocation
                });
                mapMarkers['hospitalMarker'] = hospitalMap();
            }, function (err) {
                console.log(err);
            });
        } else {
            if ($scope.map === null || angular.isUndefined($scope.map)) {
                mapMarkers['hospitalMarker'] = hospitalMap();
            }

        }
    }
    
    /* Notification Functions & Variables */
    $scope.bookingDetails = {};
    
    $scope.toasterData = null;
    var showSOSNotifAndMarker = function (bookingDetail) {
        $rootScope.notificationSound.play();
        $scope.incomingPatients[bookingDetail.id] = bookingDetail;
        $scope.incomingRequests.push(bookingDetail.id);
        putSOSMarker(bookingDetail.id , bookingDetail.user.location);
        getDuration(bookingDetail,false);

        $scope.toasterData = bookingDetail;
        
        toaster.pop('error', "Incoming SOS Request", "{template: 'notificationTemplates/incomingSosTemplate.html', data: toasterData, emergency: pickupDetail, timeout: 2000}", null, 'templateWithData');

    }
    
    var notifyPickup = function(bookingDetail){
        
        $rootScope.notificationSound.play();
        var sosMarker = mapMarkers[bookingDetail.id];
        if(!angular.isUndefined(sosMarker)){
            sosMarker.setIcon(MapUtility.getMapIcon(mapIcons.ambulanceIcon,36));
        }
        getDuration(bookingDetail,true);
        $scope.toasterData = bookingDetail;
        toaster.pop('info', "Patient Picked Up", "{template: 'notificationTemplates/pickupNotifTemplate.html', data: toasterData, timeout: 2000}", null, 'templateWithData');
    };
    
    var notifyArrival = function(bookingDetail){
        $rootScope.notificationSound.play();
        $scope.toasterData = bookingDetail;
        var sosMarker = mapMarkers[bookingDetail.id];
        if(!angular.isUndefined(sosMarker)){
            sosMarker.setMap(null);
            delete mapMarkers[bookingDetail.id];
        }
    
        
        angular.forEach($scope.incomingRequests,function(value,index){
            if(value === bookingDetail.id){
                $scope.incomingRequests.splice(index,1);
            }
        });
        $scope.availablePatients++;
        
        bookingDetail.ward = getRandomWard();
        $scope.admittedPatients[bookingDetail.id] =  angular.copy(bookingDetail);
        delete $scope.incomingPatients[bookingDetail.id];
        toaster.pop('success', "Patient Arrived", "{template: 'notificationTemplates/arrivalNotifTemplate.html', data: toasterData, timeout: 2000}", null, 'templateWithData');
    }


    $scope.showModal = function (patient) {
        $scope.currentUser = patient;
    }
    
    var subscribeToDrop = function(bookingRequest){
        var options = {
            collectionName: 'SOSRequestComplete',
            method: 'UPDATE',
            modelId: bookingRequest.id
        }
        
        PubSub.subscribe(options, function (data) {
            PubSub.unSubscribeThis(options);
            var bookingDetail = $scope.bookingDetails[bookingRequest.id];
            notifyArrival(bookingDetail);
            
        });
    }
    
    var subscribeToPickup = function(bookingRequest){
        var options = {
            collectionName: 'SOSPickupUpdate',
            method: 'UPDATE',
            modelId: bookingRequest.id
        }
        PubSub.subscribe(options, function (data) {
            PubSub.unSubscribeThis(options);
            subscribeToDrop(bookingRequest);
            var bookingDetail = $scope.bookingDetails[bookingRequest.id];
            bookingDetail.emergencyData = data.data;
            getDuration(bookingRequest,true);
            notifyPickup(bookingDetail);
        });
        
    }
    
    
    PubSub.subscribe({
        collectionName: 'SOSRequest',
        modelId: LoopBackAuth.currentUserId,
        method: "POST"
    }, function (bookingRequest) {
        if(angular.isUndefined($scope.bookingDetails[bookingRequest.id])){
            $scope.bookingDetails[bookingRequest.id] = bookingRequest;
            showSOSNotifAndMarker(bookingRequest);
            getDuration(bookingRequest,false);
            subscribeToPickup(bookingRequest);
        }
        
    });
    
    $scope.initMap();


});